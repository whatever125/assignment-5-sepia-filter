#include <image.h>
#include <math.h>
#include <stdio.h>

#define MIN(x, y) (((x) < (y)) ? (x) : (y))

void sepia(struct image* source) {
  if (source->data == NULL) return;
  struct image result = image_create(source->width, source->height);
  if (!result.valid) return;
  for (size_t i = 0; i < source->height; i ++) {
    for (size_t j = 0; j < source->width; j ++) {
      struct pixel* result_p = &result.data[i * source->width + j];
      struct pixel* source_p = &source->data[i * source->width + j];
      result_p->r = MIN((uint8_t) (source_p->r * 0.393 + source_p->g * 0.769 + source_p->b * 0.189), 255);
      result_p->g = MIN((uint8_t) (source_p->r * 0.349 + source_p->g * 0.686 + source_p->b * 0.168), 255);
      result_p->b = MIN((uint8_t) (source_p->r * 0.272 + source_p->g * 0.534 + source_p->b * 0.131), 255);
    }
  }
  image_destroy(source);
  *source = result;
}
