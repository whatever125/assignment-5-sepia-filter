#include "image.h"

struct image image_create(const uint64_t width, const uint64_t height) {
  void* allocated = malloc(sizeof(struct pixel) * width * height);
  if (!allocated) return INVALID_IMAGE;
  return (struct image) {
    .valid = true,
    .width = width,
    .height = height,
    .data = allocated
  };
}

void image_destroy(struct image *image) {
  if (image->data == NULL) return;
  free(image->data);
  image->valid = false;
  image->height = 0;
  image->width = 0;
  image->data = NULL;
}

struct pixel* image_pixel_at(struct image const image, const uint64_t width, const uint64_t height) {
  if (width >= image.width || height >= image.height) {
    return NULL;
  }
  return image.data + (height * image.width) + width;
}
