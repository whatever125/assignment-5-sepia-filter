#include "image.h"
#include <inttypes.h>
#include <stdio.h>
#include <stdlib.h>

#define BMP_TYPE 0x4D42

struct __attribute__((packed)) bmp_header
{
  uint16_t bfType;
  uint32_t  bfileSize;
  uint32_t bfReserved;
  uint32_t bOffBits;
  uint32_t biSize;
  uint32_t  biWidth;
  uint32_t  biHeight;
  uint16_t biPlanes;
  uint16_t biBitCount;
  uint32_t biCompression;
  uint32_t  biSizeImage;
  uint32_t biXPelsPerMeter;
  uint32_t biYPelsPerMeter;
  uint32_t biClrUsed;
  uint32_t biClrImportant;
};

size_t padding(uint32_t width) {
  return (4 - (width * sizeof(struct pixel)) % 4) % 4;
}

enum read_status  {
  READ_OK = 0,
  READ_INVALID_SIGNATURE,
  READ_INVALID_BITS,
  READ_INVALID_HEADER,
  READ_ERROR
};

enum read_status from_bmp(FILE* in, struct image* img) {
  struct bmp_header header = {0};
  if (fread(&header, sizeof(header), 1, in) < 1) {
    return READ_INVALID_BITS;
  }
  if (header.bfType != BMP_TYPE) {
    return READ_INVALID_SIGNATURE;
  }

  int8_t* img_data = malloc(header.biSizeImage);

  if (fread(img_data, header.biSizeImage, 1, in) < 1) {
    free(img_data);
    return READ_INVALID_BITS;
  }

  size_t pad = padding(header.biWidth);
  *img = image_create(header.biWidth, header.biHeight);
  if (!img->valid) {
    return READ_ERROR;
  }

  for (size_t i = 0; i < img->height; i++) {
    for (size_t j = 0; j < img->width; j++) {
      img->data[i * img->width + j] = (struct pixel) {
        img_data[i * (img->width * sizeof(struct pixel) + pad) + j * sizeof(struct pixel)],
        img_data[i * (img->width * sizeof(struct pixel) + pad) + j * sizeof(struct pixel) + 1],
        img_data[i * (img->width * sizeof(struct pixel) + pad) + j * sizeof(struct pixel) + 2]};
    }
  }
  free(img_data);

  return READ_OK;
}

enum write_status  {
  WRITE_OK = 0,
  WRITE_ERROR
};

const struct bmp_header sample_header =
{
  .bfType = BMP_TYPE,
  .bfReserved = 0,
  .bOffBits = sizeof(struct bmp_header),
  .biSize = 40,
  .biPlanes = 1,
  .biBitCount = 24,
  .biCompression = 0,
  .biXPelsPerMeter = 0,
  .biYPelsPerMeter = 0,
  .biClrUsed = 0,
  .biClrImportant = 0
};

struct bmp_header create_sample_header(struct image const* img) {
  struct bmp_header header = sample_header;
  header.biWidth = img->width;
  header.biHeight = img->height;
  size_t pad = padding(img->width);
  header.biSizeImage = (img->width * sizeof(struct pixel) + pad) * img->height;
  header.bfileSize = header.biSizeImage + header.bOffBits;
  return header;
}

size_t pixel_address(struct image const* img, size_t i, size_t j) {
  size_t pad = padding(img->width);
  return i * (img->width * sizeof(struct pixel) + pad) + j * sizeof(struct pixel);
}

enum write_status to_bmp(FILE* out, struct image const* img) {
  struct bmp_header header = create_sample_header(img);

  if (fwrite(&header, sizeof(header), 1, out) < 1) {
    return WRITE_ERROR;
  }

  uint8_t *output_img = malloc(header.biSizeImage);
  for (size_t i = 0; i < img->height; i++) {
    for (size_t j = 0; j < img->width; j++) {
      output_img[pixel_address(img, i, j)] = img->data[i * img->width + j].b;
      output_img[pixel_address(img, i, j) + 1] = img->data[i * img->width + j].g;
      output_img[pixel_address(img, i, j) + 2] = img->data[i * img->width + j].r;
    }
    for (size_t k = 0; k < padding(img->width); k++) {
      output_img[pixel_address(img, i, img->width) + k] = 0;
    }
  }

  if (fwrite(output_img, 1, header.biSizeImage, out) < header.biSizeImage) {
    free(output_img);
    return WRITE_ERROR;
  }

  free(output_img);
  return WRITE_OK;
}
