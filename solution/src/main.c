#include "image.h"
#include "bmpio.h"
#include "sepia.h"
#include "sepia_sse.h"

#include <stdio.h>
#include <string.h>
#include <time.h>

#define OK_CODE 0
#define ERROR_CODE (-1)
#define TEST 0

int main(int argc, char** argv) {
  if (argc != 3) {
    fprintf(stderr, "Incorrect number of arguments. Usage example: ./image-transformer <source-image> <transformed-image>\n");
    return ERROR_CODE;
  }

  struct image image = {0};

  switch (read_bmp(argv[1], &image)) {
    case READ_OK:
      break;
    case READ_INVALID_SIGNATURE:
      fprintf(stderr, "Error reading BMP content");
      image_destroy(&image);
      return ERROR_CODE;
    case READ_INVALID_BITS:
      fprintf(stderr, "Error reading file");
      image_destroy(&image);
      return ERROR_CODE;
    case READ_INVALID_HEADER:
      fprintf(stderr, "Error reading BMP header");
      image_destroy(&image);
      return ERROR_CODE;
    default:
      fprintf(stderr, "Error reading");
      image_destroy(&image);
      return ERROR_CODE;
  }

  if (TEST) {
    printf("Test started\n");
    clock_t t;
    t = clock();
    for (size_t i = 0; i < 100; i ++) {
      sepia(&image);
    }
    printf("Without SSE: %f seconds\n", ((double) (clock() - t)) / CLOCKS_PER_SEC);

    t = clock();
    for (size_t i = 0; i < 100; i ++) {
      sepia_sse(&image);
    }
    printf("With SSE: %f\n seconds\n", ((double) (clock() - t)) / CLOCKS_PER_SEC);
    printf("Done\n");
  } else {
    sepia(&image);
  }

  switch (write_bmp(argv[2], &image)) {
    case WRITE_OK:
      image_destroy(&image);
      break;
    case WRITE_ERROR:
      fprintf(stderr, "Error writing new file");
      image_destroy(&image);
      return ERROR_CODE;
    default:
      fprintf(stderr, "Error writing");
      image_destroy(&image);
      return ERROR_CODE;
  }

  return OK_CODE;
}
