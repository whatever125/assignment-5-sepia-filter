%macro int_to_packed_float 2
  xor       rax, rax
  mov       al, %1
  shl       rax, 32
  mov       al, %1
  movq      mm0, rax
  cvtpi2ps  %2, mm0
  movlhps   %2, %2
%endmacro

section .text
global linear_pixel_transformation
linear_pixel_transformation:
  int_to_packed_float [rdi], xmm0
  mulps xmm0, [rdx]
  int_to_packed_float [rdi + 1], xmm1
  mulps xmm1, [rcx]
  int_to_packed_float [rdi + 2], xmm2
  mulps xmm2, [r8]

  addps     xmm0, xmm1
  addps     xmm0, xmm2
  cvtps2dq  xmm0, xmm0
  packusdw  xmm0, xmm0
  packuswb  xmm0, xmm0

  sub   rsp, 16

  movd  [rsp], xmm0
  mov   ax, word [rsp]
  mov   word [rsi], ax
  mov   al, byte [rsp + 2]
  mov   byte [rsi + 2], al
  
  add   rsp, 16

  ret
