#include <image.h>
#include <linear_pixel_transformation.h>
#include <math.h>
#include <stdio.h>
#include <xmmintrin.h>

static const float sepia_red_vector[3] __attribute__((aligned(16)))   = {0.393f, 0.769f, 0.189f};
static const float sepia_green_vector[3] __attribute__((aligned(16))) = {0.349f, 0.686f, 0.168f};
static const float sepia_blue_vector[3] __attribute__((aligned(16)))  = {0.272f, 0.534f, 0.131f};

void sepia_sse(struct image* source) {
  if (source->data == NULL) return;
  struct image result = image_create(source->width, source->height);
  if (!result.valid) return;
  for (size_t i = 0; i < source->height; i ++) {
    for (size_t j = 0; j < source->width; j ++) {
      struct pixel* result_p = &result.data[i * source->width + j];
      struct pixel* source_p = &source->data[i * source->width + j];
      linear_pixel_transformation(source_p, result_p, sepia_red_vector, sepia_green_vector, sepia_blue_vector);
    }
  }
  image_destroy(source);
  *source = result;
}
