#ifndef SEPIA_H
#define SEPIA_H

#include <image.h>
#include <math.h>
#include <stdio.h>

void sepia(struct image* source);

#endif
