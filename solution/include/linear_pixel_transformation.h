#ifndef LINEAR_PIXEL_TRANSFORMATION_H
#define LINEAR_PIXEL_TRANSFORMATION_H

extern void linear_pixel_transformation(struct pixel* source_pixel, struct pixel* result_pixel, const float red_vector[3], const float green_vector[3], const float blue_vector[3]);

#endif
