#ifndef SEPIA_SSE_H
#define SEPIA_SSE_H

#include <image.h>
#include <linear_pixel_transformation.h>
#include <math.h>
#include <stdio.h>
#include <xmmintrin.h>

void sepia_sse(struct image* source);

#endif
