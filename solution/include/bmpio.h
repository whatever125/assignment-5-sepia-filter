#ifndef BMPIO_H
#define BMPIO_H

#include <stdio.h>

#include <bmp.h>

enum read_status read_bmp(const char* filename, struct image* img);
enum write_status write_bmp(const char* filename, struct image* img);

#endif
